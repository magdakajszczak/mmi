function isPowerOf2(value) {
    return (value & (value - 1)) === 0;
}

class ImageLoader {
    constructor(gl) {
        this.gl = gl;
    }

    fillTextureWithSinglePixel(staticTextureData){
        const gl = this.gl;
        const pixel = new Uint8Array([0, 0, 255, 255]);
        const width = 1;
        const height = 1;
        const border = 0;
        gl.bindTexture(gl.TEXTURE_2D, staticTextureData.texture);
        gl.texImage2D(gl.TEXTURE_2D,
            staticTextureData.level,
            staticTextureData.internalFormat,
            width,
            height,
            border,
            staticTextureData.srcFormat,
            staticTextureData.srcType,
            pixel);
    }

    fillTextureWithImage(staticTextureData, image) {
        const gl = this.gl;
        return function() {
            gl.bindTexture(gl.TEXTURE_2D, staticTextureData.texture);
            gl.texImage2D(gl.TEXTURE_2D,
                staticTextureData.level,
                staticTextureData.internalFormat,
                staticTextureData.srcFormat,
                staticTextureData.srcType,
                image);

            if (isPowerOf2(image.width) && isPowerOf2(image.height)) {
                // Yes, it's a power of 2. Generate mips.
                gl.generateMipmap(gl.TEXTURE_2D);
            } else {

                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
            }
        };
    }

    loadImageIntoTexture(url) {
        const gl = this.gl;
        const texture = gl.createTexture();
        const staticTextureData = new Texture(texture, gl.RGBA, gl.UNSIGNED_BYTE);
        this.fillTextureWithSinglePixel(staticTextureData);
        const image = new Image();
        image.onload = this.fillTextureWithImage(staticTextureData, image);
        image.src = url;
        return texture;
    }
}