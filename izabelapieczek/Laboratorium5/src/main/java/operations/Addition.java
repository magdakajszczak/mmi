package operations;

public class Addition implements OperationStrategy {
    @Override
    public double count(double firstNumber, double secondNumber) {
        return firstNumber + secondNumber;
    }
}
