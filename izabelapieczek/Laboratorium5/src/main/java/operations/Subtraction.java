package operations;

public class Subtraction implements OperationStrategy {
    @Override
    public double count(double firstNumber, double secondNumber) {
        return firstNumber - secondNumber;
    }
}
