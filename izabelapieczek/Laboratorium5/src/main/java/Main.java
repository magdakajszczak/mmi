import exceptions.DivisionByZeroException;
import userInterface.MenuInterface;
import userInterface.MenuLogic;

import static userInterface.MenuInterface.*;

public class Main {
    public static void main(String[] args) throws DivisionByZeroException {
        MenuLogic menuLogic = new MenuLogic();
        MenuInterface menuInterface = new MenuInterface();
        menuInterface.printGreeting();
        menuInterface.printInstruction();
        menuLogic.enterNumbers();
        menuInterface.printEnteredValues(menuLogic.getNumberOne(), menuLogic.getNumberTwo());
        menuInterface.printSelectOperation();
        menuLogic.selectOperation();
        menuInterface.printResult(menuLogic.getResult());
        menuInterface.printFarewell();
    }
}
