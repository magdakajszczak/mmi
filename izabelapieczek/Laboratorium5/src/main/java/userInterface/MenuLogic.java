package userInterface;

import exceptions.DivisionByZeroException;
import operations.*;

import java.util.Scanner;

public class MenuLogic {

    private double numberOne;
    private double numberTwo;
    private OperationStrategy strategy;
    private double result;

    public void enterNumbers() {
        Scanner input = new Scanner(System.in);
        setNumberOne(input.nextInt());
        setNumberTwo(input.nextInt());
    }

    public void selectOperation() throws DivisionByZeroException {
        int choice = makeChoice();

        while (strategy == null) {
            setupStrategy(choice);
        }

        result = this.strategy.count(numberOne, numberTwo);
    }

    private int makeChoice() {
        Scanner input = new Scanner(System.in);
        return input.nextInt();
    }

    public void setupStrategy(int choice) {
        switch (choice) {
            case 1:
                this.strategy = new Addition();
                break;
            case 2:
                this.strategy = new Subtraction();
                break;
            case 3:
                this.strategy = new Multiplication();
                break;
            case 4:
                this.strategy = new Division();
                break;
            default:
                System.out.println("There is no such operation.");
                setupStrategy(makeChoice());
        }
    }

    public void setNumberOne(double numberOne) {
        this.numberOne = numberOne;
    }

    public void setNumberTwo(double numberTwo) {
        this.numberTwo = numberTwo;
    }

    public double getNumberOne() {
        return numberOne;
    }

    public double getNumberTwo() {
        return numberTwo;
    }

    public OperationStrategy getStrategy() {
        return strategy;
    }

    public void setStrategy(OperationStrategy strategy) {
        this.strategy = strategy;
    }

    public double getResult() {
        return result;
    }

    public void setResult(double result) {
        this.result = result;
    }

}
