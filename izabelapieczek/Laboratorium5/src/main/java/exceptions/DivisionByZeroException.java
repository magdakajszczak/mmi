package exceptions;

public class DivisionByZeroException extends Exception {
    public DivisionByZeroException() {
        System.out.println("CANNOT DIVIDE BY ZERO!!!");
    }
}
