package userInterface;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import static junit.framework.TestCase.assertEquals;

public class MenuInterfaceTest {

    MenuInterface menuInterface;

    @Before
    public void init() {
        this.menuInterface = new MenuInterface();
    }

    @Test
    public void shouldPrintFarewell() {
        OutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        System.setOut(ps);

        menuInterface.printFarewell();
        assertEquals("Thanks for using Calculator!\r\n", os.toString());

        PrintStream originalOut = System.out;
        System.setOut(originalOut);
    }

    @Test
    public void shouldPrintGreeting() {
        OutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        System.setOut(ps);

        menuInterface.printGreeting();
        assertEquals("|||||||||||||||||||||||\r\n|||Simple Calculator|||\r\n|||||||||||||||||||||||\r\n", os.toString());

        PrintStream originalOut = System.out;
        System.setOut(originalOut);
    }

    @Test
    public void shouldEnteredValues() {
        OutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        System.setOut(ps);

        double firstNumber = 15.0;
        double secondNumber = 10.0;

        menuInterface.printEnteredValues(firstNumber,secondNumber);
        assertEquals("|||||||||||||||||||||||\r\n" +
                "|||||Entered Values||||\r\n" +
                "First: " + firstNumber + "\r\n" +
                "Second: " + secondNumber + "\r\n" +
                "|||||||||||||||||||||||" + "\r\n", os.toString());


        PrintStream originalOut = System.out;
        System.setOut(originalOut);
    }

    @Test
    public void shouldPrintInstruction() {
        OutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        System.setOut(ps);

        menuInterface.printInstruction();

        assertEquals("||||||INSTRUCTION||||||\r\n" +
                "|||||||||||||||||||||||\r\n" +
                "|||||Enter numbers|||||\r\n", os.toString());

        PrintStream originalOut = System.out;
        System.setOut(originalOut);
    }

    @Test
    public void shouldPrintResult() {
        OutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        System.setOut(ps);
        double result = 5.0;
        menuInterface.printResult(result);

        assertEquals("|||||||||||||||||||||||\r\n" +
                "||||||| RESULT ||||||||\r\n" +
                "|||||||||||||||||||||||\r\n" +
                result + "\r\n", os.toString());

        PrintStream originalOut = System.out;
        System.setOut(originalOut);
    }

    @Test
    public void shouldPrintSelectOperation() {
        OutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        System.setOut(ps);

        menuInterface.printSelectOperation();
        assertEquals("||||Select operation|||\r\n" +
                "|||||||||||||||||||||||\r\n" +
                "||||| 1 - Addition|||||\r\n" +
                "|| 2 for Subtraction|||\r\n" +
                "| 3 for Multiplication|\r\n" +
                "|||| 4 for Division||||\r\n", os.toString());

        PrintStream originalOut = System.out;
        System.setOut(originalOut);
    }
}