package operations;

import exceptions.DivisionByZeroException;
import org.junit.Before;
import org.junit.Test;
import junit.framework.Assert;

import static junit.framework.TestCase.assertEquals;

public class DivisionTest {

    private OperationStrategy strategy;

    @Before
    public void init(){
        this.strategy = new Division();
    }

    @Test
    public void shouldReturnTrueIfProperResultOfDivision() throws DivisionByZeroException {
        assertEquals(2.0,strategy.count(10,5));
    }

    @Test(expected = DivisionByZeroException.class)
    public void shouldReturnThrowExceptionWhenSecondElementIsZero() throws DivisionByZeroException {
        strategy.count(10,0);
    }
}