package operations;


import exceptions.DivisionByZeroException;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class MultiplicationTest {

    private OperationStrategy strategy;

    @Before
    public void init() {
        this.strategy = new Multiplication();
    }

    @Test
    public void shouldReturnTrueIfProperResultOfMultiplication() throws DivisionByZeroException {
        assertEquals(25.0, strategy.count(5, 5));
    }
}