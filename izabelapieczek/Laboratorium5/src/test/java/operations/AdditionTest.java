package operations;

import exceptions.DivisionByZeroException;
import org.junit.Before;
import org.junit.Test;
import static junit.framework.TestCase.assertEquals;

public class AdditionTest {

    private OperationStrategy strategy;

    @Before
    public void init(){
        this.strategy = new Addition();
    }

    @Test
    public void shouldReturnTrueIfProperResultOfAddition() throws DivisionByZeroException {
        assertEquals(10.0,strategy.count(5,5));
    }
}