package com.company;

import java.time.LocalTime;

public class CustPrintCurrentTime implements PrintCurrentTime {
    @Override
    public String print() {
        return LocalTime.now().toString();
    }
}
