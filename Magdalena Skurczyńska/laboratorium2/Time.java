package com.company;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;


class Time implements PrintCurrentTime {
    public String print(){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);
    }
}